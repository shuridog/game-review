import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class EventTriggerService {

  private eventSubject = new BehaviorSubject<any>(false);

  triggerEvent(param: any) {
      this.eventSubject.next(param);
  }
 
  getEventSubject(): BehaviorSubject<any> {
     return this.eventSubject;
  }
}
