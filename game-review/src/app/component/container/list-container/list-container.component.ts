import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { GameReviewService } from 'src/app/service/game-review.service';
import { EventTriggerService } from 'src/app/service/event-trigger.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-list-container',
  templateUrl: './list-container.component.html',
  styleUrls: ['./list-container.component.scss']
})
export class ListContainerComponent implements OnDestroy, OnInit {
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();

  gameReviewData: any;
  constructor(
    private gameReviewService: GameReviewService,
    private eventTriggerService: EventTriggerService
  ) { }

  ngOnInit(): void {
    this.eventTriggerService.getEventSubject().subscribe((param: any) => {
      if (param !== false) {
        this.rerender();
      }
    });
    
    this.dtOptions = {
      pagingType: 'first_last_numbers',
      pageLength: 5,
      lengthMenu: [ 5, 10, 50 ]
    };
    
    this.gameReviewService.getGameReview().subscribe(
      response => {
        this.gameReviewData = response;
        this.dtTrigger.next();
      }
    );

  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtOptions.destroy = true;
  }
}
