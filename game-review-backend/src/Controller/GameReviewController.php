<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Request\ParamFetcherInterface;

/**
 * @Rest\Route("/game-reviews")
 */
class GameReviewController extends AbstractController
{

    const FILE_PATH = '/game-review-backend/src/data/review.json';
    /**
     * @Rest\Get("/api")
     */
    public function getGameReview() : JsonResponse {
        $file = file_get_contents(self::FILE_PATH);
        return new JsonResponse(json_decode($file));
    }

    /**
     * @Rest\Post("/api")
     * @RequestParam(name="fullName", nullable=true)
     * @RequestParam(name="email", nullable=true)
     * @RequestParam(name="rating", nullable=true)
     * @RequestParam(name="review", nullable=true)
     */
    public function postGameReview(ParamFetcherInterface $paramFetcher): JsonResponse {
        $params = $paramFetcher->all();
        $file = json_decode(file_get_contents(self::FILE_PATH), true);
        $newArr = array_push($file, $params);

        $encodedFile = json_encode($file);
        file_put_contents(self::FILE_PATH, $encodedFile);

        return new JsonResponse(1);
    }
}